# Notification delivery microservice (Sample)

This microservice was intended to deliver notifications through different providers. It receives a target, the desired notification to be sent and one of the supported providers.

This project currently supports notification delivery via Telegram, One Signal and e-mail through Sendgrid.


## How to run

Setup a .env file according to the given example and run the following commands in your command line.

```
npm install
npm run seed
npm start
```

