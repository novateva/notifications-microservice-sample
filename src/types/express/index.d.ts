import { Document, Model } from 'mongoose'
import { IApiKey } from '../../interfaces/IApiKey'
import { INotification } from '../../interfaces/INotification'
import { IProvider } from '../../interfaces/IProvider'
import { ITarget } from '../../interfaces/ITarget'
import { IUser } from '../../interfaces/IUser'
declare global {
  namespace Express {
    export interface Request {
      currentUser: IUser & Document
    }
  }

  namespace Models {
    export type UserModel = Model<IUser & Document>
    export type ApiKeyModel = Model<IApiKey & Document>
    export type NotificationModel = Model<INotification & Document>
    export type TargetModel = Model<ITarget & Document>
    export type ProviderModel = Model<IProvider & Document>
  }
}

declare module 'http' {
  interface IncomingHttpHeaders {
    custom_token?: string
  }
}
