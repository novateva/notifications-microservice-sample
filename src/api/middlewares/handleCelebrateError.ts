import { CelebrateError, isCelebrateError } from 'celebrate'
import { Response } from 'express'

export default (err: CelebrateError, req, res: Response, next) => {
  if (isCelebrateError(err)) {
    const bodyDetails = err.details.get('body')?.details.map(err => err.message) ?? []
    const paramsDetails = err.details.get('params')?.details.map(err => err.message) ?? []

    const details = [...bodyDetails, ...paramsDetails]

    return res.status(422).send({
      message: err.message,
      details,
    })
  }

  return next(err)
}
