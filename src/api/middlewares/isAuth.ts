import { Request } from 'express'
import jwt from 'express-jwt'
import { NextFunction, Response } from 'express-serve-static-core'
import { isValidObjectId } from 'mongoose'
import Container from 'typedi'
import config from '../../config'
import CryptoJS from 'crypto-js'

/**
 * We are assuming that the JWT will come in a header with the form
 *
 * Authorization: Bearer ${JWT}
 *
 * But it could come in a query parameter with the name that you want like
 * GET https://my-bulletproof-api.com/stats?apiKey=${JWT}
 * Luckily this API follow _common sense_ ergo a _good design_ and don't allow that ugly stuff
 */
const getJwtTokenFromHeader = (req: Request) => {
  /**
   * @TODO Edge and Internet Explorer do some weird things with the headers
   * So I believe that this should handle more 'edge' cases ;)
   */
  if (
    (req.headers.authorization &&
      req.headers.authorization.split(' ')[0] === 'Token') ||
    (req.headers.authorization &&
      req.headers.authorization.split(' ')[0] === 'Bearer')
  ) {
    return req.headers.authorization.split(' ')[1]
  }
  return null
}

const getCustomTokenFromHeader = (req: Request) => {
  return req.headers.custom_token ?? ''
}

const isAuthJwt = jwt({
  secret: config.jwtSecret, // The _secret_ to sign the JWTs
  algorithms: ['sha1', 'RS256', 'HS256'], // JWT Algorithm
  userProperty: 'token', // Use req.token to store the JWT
  getToken: getJwtTokenFromHeader, // How to extract the JWT from the request
})

const isAuthCustom = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const token = getCustomTokenFromHeader(req)

  const decrypted = CryptoJS.AES.decrypt(token, config.appSecret).toString(
    CryptoJS.enc.Utf8,
  )

  if (isValidObjectId(decrypted)) {
    const apiKeyModel = Container.get('apiKeyModel') as Models.ApiKeyModel
    const item = await apiKeyModel.findById(decrypted)
    if (item && item.status === 'ACTIVE') return next()
  }

  return res.status(401).send({
    message: 'Invalid custom token',
  })
}

export default (req: Request, res: Response, next: NextFunction) => {
  if (getJwtTokenFromHeader(req)) return isAuthJwt(req, res, next)

  return isAuthCustom(req, res, next)
}
