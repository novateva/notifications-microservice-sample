import attachCurrentUser from './attachCurrentUser'
import isAuth from './isAuth'
import handleCelebrateError from './handleCelebrateError'

export default {
  attachCurrentUser,
  isAuth,
  handleCelebrateError,
}
