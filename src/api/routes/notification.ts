import { celebrate, Joi } from 'celebrate'
import { Router, Request, Response, NextFunction } from 'express'
import Container from 'typedi'
import { Logger } from 'winston'
import NotificationService from '../../services/notification'
import middlewares from '../middlewares'

const route = Router()

export default (app: Router) => {
  app.use('/notifications', route)

  route.post(
    '/',
    celebrate(
      {
        body: Joi.object({
          _notification_id: Joi.string(),
          title: Joi.string().required(),
          type: Joi.string().valid('email', 'telegram', 'push').required(),
          status: Joi.string()
            .valid('going', 'paused', 'error', 'deleted')
            .required(),
          data: Joi.object().required(),
          body_requested: Joi.string(),
          error_logs: Joi.string(),
          body: Joi.string().required(),
          template_id: Joi.string(),
          internal_user_id: Joi.number(),
          executer: Joi.string(),
          providers: Joi.array().items(Joi.string().min(12).max(24)).optional(),
          targets: Joi.array().optional(),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Notification create: %o', req.body)
      try {
        const notificationServiceInstance = Container.get(NotificationService)

        const response = await notificationServiceInstance.create(req.body)

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.put(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
          _notification_id: Joi.string(),
          title: Joi.string().required(),
          type: Joi.string().valid('email', 'telegram', 'push').required(),
          status: Joi.string()
            .valid('going', 'paused', 'error', 'deleted')
            .required(),
          data: Joi.object().required(),
          body_requested: Joi.string(),
          error_logs: Joi.string(),
          body: Joi.string().required(),
          template_id: Joi.string().required(),
          internal_user_id: Joi.number(),
          executer: Joi.string(),
          providers: Joi.array().items(Joi.string().min(12).max(24)).optional(),
          target: Joi.array().items(Joi.string().min(12).max(24)).optional(),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Notification update: %o', req.body)
      try {
        const notificationServiceInstance = Container.get(NotificationService)

        const { id, ...toUpdate } = req.body

        const response = await notificationServiceInstance.update(id, toUpdate)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response).status(200)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.delete(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Notification delete: %o', req.body)
      try {
        const notificationServiceInstance = Container.get(NotificationService)

        const { id } = req.body

        const response = await notificationServiceInstance.delete(id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get(
    '/:id',
    celebrate(
      {
        params: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Notification get %o', req.params)
      try {
        const notificationServiceInstance = Container.get(NotificationService)

        const response = await notificationServiceInstance.show(req.params.id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get('/', async (_req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger')
    logger.debug('Notification get all')
    try {
      const notificationServiceInstance = Container.get(NotificationService)

      const response = await notificationServiceInstance.index()

      return res.json(response)
    } catch (e) {
      logger.error('Error: %o', e)
      return next(e)
    }
  })
}
