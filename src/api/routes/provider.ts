import { celebrate, Joi } from 'celebrate'
import { Router, Request, Response, NextFunction } from 'express'
import Container from 'typedi'
import { Logger } from 'winston'
import ProviderService from '../../services/provider'
import middlewares from '../middlewares'

const route = Router()

export default (app: Router) => {
  app.use('/providers', route)

  route.post(
    '/',
    celebrate(
      {
        body: Joi.object({
          name: Joi.string().required(),
          executer: Joi.string().required(),
          provider_configuration: Joi.object().required(),
          target: Joi.string().optional().min(12).max(24),
          notification: Joi.string().optional().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Provider create: %o', req.body)
      try {
        const providerServiceInstance = Container.get(ProviderService)

        const response = await providerServiceInstance.create(req.body)

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.put(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
          name: Joi.string().required(),
          executer: Joi.string().required(),
          provider_configuration: Joi.object().required(),
          target: Joi.string().optional().min(12).max(24),
          notification: Joi.string().optional().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Provider update: %o', req.body)
      try {
        const providerServiceInstance = Container.get(ProviderService)

        const { id, ...toUpdate } = req.body

        const response = await providerServiceInstance.update(id, toUpdate)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response).status(200)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.delete(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Provider delete: %o', req.body)
      try {
        const providerServiceInstance = Container.get(ProviderService)

        const { id } = req.body

        const response = await providerServiceInstance.delete(id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get(
    '/:id',
    celebrate(
      {
        params: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Provider get %o', req.params)
      try {
        const providerServiceInstance = Container.get(ProviderService)

        const response = await providerServiceInstance.show(req.params.id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get('/', async (_req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger')
    logger.debug('Provider get all')
    try {
      const providerServiceInstance = Container.get(ProviderService)

      const response = await providerServiceInstance.index()

      return res.json(response)
    } catch (e) {
      logger.error('Error: %o', e)
      return next(e)
    }
  })
}
