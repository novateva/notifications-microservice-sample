import { celebrate, Joi } from 'celebrate'
import { Router, Request, Response, NextFunction } from 'express'
import Container from 'typedi'
import { Logger } from 'winston'
import UserService from '../../services/user'
import middlewares from '../middlewares'
const route = Router()

export default (app: Router) => {
  app.use('/users', route)

  route.get(
    '/me',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    (req: Request, res: Response) => {
      return res.json({ user: req.currentUser }).status(200)
    },
  )

  route.post(
    '/',
    celebrate(
      {
        body: Joi.object({
          username: Joi.string().required(),
          email: Joi.string().required(),
          password: Joi.string().required(),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('User create: %o', req.body)
      try {
        const userServiceInstance = Container.get(UserService)

        const response = await userServiceInstance.create(req.body)

        return res.json(response).status(200)
      } catch (e) {
        logger.error('🔥 error: %o', e)
        return next(e)
      }
    },
  )

  route.put(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
          username: Joi.string().required(),
          email: Joi.string().required(),
          password: Joi.string().optional(),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('User update: %o', req.body)
      try {
        const userServiceInstance = Container.get(UserService)

        const { id, ...toUpdate } = req.body

        const response = await userServiceInstance.update(id, toUpdate)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response).status(200)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.delete(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('User delete: %o', req.body)
      try {
        const userServiceInstance = Container.get(UserService)

        const { id } = req.body

        const response = await userServiceInstance.delete(id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get(
    '/:id',
    celebrate(
      {
        params: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('User get %o', req.params)
      try {
        const userServiceInstance = Container.get(UserService)

        const response = await userServiceInstance.show(req.params.id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get('/', async (_req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger')
    logger.debug('User get all')
    try {
      const userServiceInstance = Container.get(UserService)

      const response = await userServiceInstance.index()

      return res.json(response)
    } catch (e) {
      logger.error('Error: %o', e)
      return next(e)
    }
  })
}
