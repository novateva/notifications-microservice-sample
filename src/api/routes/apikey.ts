import { celebrate, Joi } from 'celebrate'
import { Router, Request, Response, NextFunction } from 'express'
import Container from 'typedi'
import { Logger } from 'winston'
import ApiKeyService from '../../services/apikey'
import middlewares from '../middlewares'

const route = Router()

export default (app: Router) => {
  app.use('/api_key', route)

  route.post(
    '/',
    celebrate(
      {
        body: Joi.object({
          user: Joi.string().required().min(12).max(24),
          notifications: Joi.array()
            .items(Joi.string().min(12).max(24))
            .optional(),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('ApiKey create: %o', req.body)
      try {
        const apikeyServiceInstance = Container.get(ApiKeyService)

        const response = await apikeyServiceInstance.create(req.body)

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.delete(
    '/',
    celebrate(
      {
        body: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('ApiKey delete: %o', req.body)
      try {
        const apikeyServiceInstance = Container.get(ApiKeyService)

        const { id } = req.body

        const response = await apikeyServiceInstance.delete(id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get(
    '/:id',
    celebrate(
      {
        params: Joi.object({
          id: Joi.string().required().min(12).max(24),
        }),
      },
      { abortEarly: false },
    ),
    middlewares.handleCelebrateError,
    middlewares.isAuth,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger')
      logger.debug('Apikey get %o', req.params)
      try {
        const apikeyServiceInstance = Container.get(ApiKeyService)

        const response = await apikeyServiceInstance.show(req.params.id)

        if (response === null)
          return res.status(404).json({ message: 'not found' })

        return res.json(response)
      } catch (e) {
        logger.error('Error: %o', e)
        return next(e)
      }
    },
  )

  route.get('/', async (_req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger')
    logger.debug('ApiKey get all')
    try {
      const apikeyServiceInstance = Container.get(ApiKeyService)

      const response = await apikeyServiceInstance.index()

      return res.json(response)
    } catch (e) {
      logger.error('Error: %o', e)
      return next(e)
    }
  })
}
