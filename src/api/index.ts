import { Router } from 'express'
import auth from './routes/auth'
import user from './routes/user'
import agendash from './routes/agendash'
import provider from './routes/provider'
import notification from './routes/notification'
import apikey from './routes/apikey'

// guaranteed to get dependencies
export default () => {
  const app = Router()
  auth(app)
  user(app)
  provider(app)
  notification(app)
  apikey(app)
  agendash(app)

  return app
}
