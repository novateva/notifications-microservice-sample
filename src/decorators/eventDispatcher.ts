/**
 * Originally taken from 'w3tecch/express-typescript-boilerplate'
 * Credits to the author
 */
import { Container } from 'typedi'
import { dispatcher } from '../loaders/events'

export function EventDispatcher() {
  return (object: any, propertyName: string, index?: number): void => {
    Container.registerHandler({ object, propertyName, index, value: () => dispatcher })
  }
}

export { EventDispatcher as EventDispatcherInterface } from '@foxandfly/ts-event-dispatcher'
