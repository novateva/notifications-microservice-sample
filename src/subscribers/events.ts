export default {
  user: {
    signUp: 'onUserSignUp',
    signIn: 'onUserSignIn',
  },
  notification: {
    create: 'onNotificationCreate',
  },
}
