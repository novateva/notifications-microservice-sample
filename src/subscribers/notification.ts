import { Container } from 'typedi'
import events from './events'
import { INotification } from '../interfaces/INotification'
import { Logger } from 'winston'
import { EventDispatcherInterface } from '../decorators/eventDispatcher'
import { Document } from 'mongoose'
import { IProvider } from '../interfaces/IProvider'
import ProviderHandler from '../services/providers/ProviderHandler'

export const NotificationSubscriber = (
  dispatcher: EventDispatcherInterface<any>,
) => {
  //Add every event with addListener
  dispatcher.addListener(
    events.notification.create,
    async (notification: INotification & Document) => {
      const Logger: Logger = Container.get('logger')
      const sendgridProvider: ProviderHandler = Container.get('emailProvider')
      const oneSignalProvider: ProviderHandler = Container.get('pushProvider')
      const telegramProvider: ProviderHandler =
        Container.get('telegramProvider')

      try {
        const providers = {
          sendgrid: sendgridProvider,
          onesignal: oneSignalProvider,
          telegram: telegramProvider,
        }

        const sensedNotifs = (notification.providers as IProvider[]).map(
          provider => {
            if (!(provider.name in providers))
              throw Error(`Invalid provider name: ${provider.name}`)
            return providers[provider.name].sendNotification(notification)
          },
        )

        await Promise.all(sensedNotifs)
      } catch (e) {
        Logger.error(
          `Error on event ${events.notification.create}: %j`,
          e?.response || e,
        )
        notification.status = 'error'
        await notification.save()
        throw e
      }
    },
  )
}
