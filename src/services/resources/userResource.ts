import { Document } from 'mongoose'
import { IUser } from '../../interfaces'

type User = IUser & Document

const _userResource = (userRecord: User) => {
  const user = userRecord.toObject()
  Reflect.deleteProperty(user, 'password')
  Reflect.deleteProperty(user, 'salt')
  return user
}

const _userResourceArray = (userRecords: User[]) =>
  userRecords.map(_userResource)

export default function (input: User | User[]) {
  if (Array.isArray(input)) return _userResourceArray(input)

  return _userResource(input)
}
