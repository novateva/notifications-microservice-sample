import { IProviderInputDTO } from '../interfaces/IProvider'
import { Inject, Service } from 'typedi'

@Service()
export default class ProviderService {
  constructor(@Inject('providerModel') private providerModel) {}

  async create(provider: IProviderInputDTO) {
    const providerRecord = await this.providerModel.create(provider)
    return providerRecord.toObject()
  }

  async update(id: string, provider: IProviderInputDTO) {
    const providerRecord = await this.providerModel.findByIdAndUpdate(
      id,
      provider,
      { new: true },
    )
    return providerRecord.toObject()
  }

  async delete(id: string) {
    const providerRecord = await this.providerModel.findByIdAndDelete(id)
    return providerRecord.toObject()
  }

  async index() {
    const records = await this.providerModel.find({})
    return records.map(record => record.toObject())
  }

  async show(id: string) {
    const providerRecord = await this.providerModel.findById(id)
    return providerRecord.toObject()
  }
}
