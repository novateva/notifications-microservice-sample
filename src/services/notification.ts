import { INotificationInputDTO } from '../interfaces/INotification'
import { Inject, Service } from 'typedi'
import {
  EventDispatcher,
  EventDispatcherInterface,
} from '../decorators/eventDispatcher'
import events from '../subscribers/events'
import { ObjectId } from 'mongodb'
@Service()
export default class NotificationService {
  constructor(
    @Inject('notificationModel') private notificationModel,
    @Inject('providerModel') private providerModel: Models.ProviderModel,
    @Inject('targetModel') private targetModel: Models.TargetModel,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface<any>,
  ) {}

  async create(notification: INotificationInputDTO) {
    if (!notification.providers) notification.providers = [notification.type]

    const providerMap = {
      push: 'onesignal',
      email: 'sendgrid',
      telegram: 'telegram',
    }
    for (let i = 0; i < notification.providers.length; i++) {
      const provider = notification.providers[i]
      if (ObjectId.isValid(provider as string)) continue
      const providerRecord = await this.providerModel.findOne({
        name: providerMap[notification.type],
      })
      notification.providers[i] = providerRecord._id
    }

    if (!notification.targets) notification.targets = []

    for (let i = 0; i < notification.targets.length; i++) {
      const target = notification.targets[i]
      if (ObjectId.isValid(target as string)) continue

      const address = typeof target == 'string' ? target : target.address
      const name = target.name

      let targetRecord = await this.targetModel.findOne({
        id_on_provider: address,
      })
      if (!targetRecord) {
        targetRecord = await this.targetModel.create({
          id_on_provider: address,
          name,
        })
      }
      notification.targets[i] = targetRecord._id
    }

    const notificationRecord = await this.notificationModel.create(notification)
    try {
      await notificationRecord
        .populate('targets')
        .populate('providers')
        .execPopulate()
      await this.eventDispatcher.dispatch(
        events.notification.create,
        notificationRecord,
      )
    } catch (error) {
      console.log(error)
    }

    return notificationRecord
  }

  async update(id: string, notification: INotificationInputDTO) {
    const notificationRecord = await this.notificationModel.findByIdAndUpdate(
      id,
      notification,
      {
        new: true,
      },
    )
    return notificationRecord
  }

  async delete(id: string) {
    const notificationRecord = await this.notificationModel.findByIdAndDelete(
      id,
    )
    return notificationRecord
  }

  async index() {
    const records = await this.notificationModel.find({})
    return records
  }

  async show(id: string) {
    const notificationRecord = await this.notificationModel.findById(id)
    return notificationRecord
  }
}
