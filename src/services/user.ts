import { IUserInputDTO } from '../interfaces/IUser'
import Container, { Inject, Service } from 'typedi'
import userResource from './resources/userResource'
import { randomBytes } from 'crypto'
import { Logger } from 'winston'
import argon2 from 'argon2'

@Service()
export default class UserService {
  constructor(@Inject('userModel') private userModel: Models.UserModel) {}

  async create(user: IUserInputDTO) {
    const logger: Logger = Container.get('logger')
    try {
      const salt = randomBytes(32)
      const hashedPassword = await argon2.hash(user.password, { salt })
      const userRecord = await this.userModel.create({
        ...user,
        salt: salt.toString('hex'),
        password: hashedPassword,
      })

      return userResource(userRecord)
    } catch (e) {
      logger.error(e)
      throw e
    }
  }

  async update(id: string, user: IUserInputDTO) {
    const logger: Logger = Container.get('logger')
    try {
      const userRecord = await this.userModel.findById(id)

      const { salt: strSalt } = userRecord
      const salt = Buffer.from(strSalt, 'utf-8')

      const password = user?.password
        ? await argon2.hash(user.password, { salt })
        : userRecord.password

      userRecord.username = user.username
      userRecord.email = user.email
      userRecord.password = password

      await userRecord.save()
      return userResource(userRecord)
    } catch (error) {
      logger.error(error)
      throw error
    }
  }

  async delete(id: string) {
    const userRecord = await this.userModel.findByIdAndDelete(id)
    return userResource(userRecord)
  }

  async index() {
    const records = await this.userModel.find({})
    return userResource(records)
  }

  async show(id: string) {
    const userRecord = await this.userModel.findById(id)
    return userResource(userRecord)
  }
}
