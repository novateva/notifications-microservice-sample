import { MailService } from '@sendgrid/mail'
import Container from 'typedi'
import { INotification } from '../../interfaces/INotification'
import { ITarget } from '../../interfaces/ITarget'
import ProviderHandler from './ProviderHandler'

/**
 * Class to send SendGrid notifications, init() needs to be called before sending notifications
 */
export default class SendgridProvider extends ProviderHandler {
  private _sgMail: MailService

  async init() {
    const ProviderModel = Container.get('providerModel') as Models.ProviderModel
    const item = await ProviderModel.findOne({
      name: 'sendgrid',
    })

    if (!item) throw new Error('SendGrid provider record not found')

    const { SENDGRID_ACCESS_TOKEN } = item.provider_configuration
    const client = new MailService()
    client.setApiKey(SENDGRID_ACCESS_TOKEN)
    this._sgMail = client
  }

  async sendNotification(notification: INotification): Promise<boolean> {
    if (!this._sgMail)
      throw new Error(
        'Class not initialized, init() needs to be called before sending notifications',
      )

    const receivers = (notification.targets as ITarget[])?.map(
      target => target.id_on_provider,
    )

    await this._sgMail.send({
      to: receivers,
      from: 'onboarding@solidusotc.com',
      templateId: notification.template_id,
      dynamicTemplateData: notification.data,
    })
    return true
  }
}
