import { INotification, ITarget } from '../../interfaces'
import ProviderHandler from './ProviderHandler'
import { Client } from 'onesignal-node'
import Container from 'typedi'

/**
 * Class to send OneSignal notifications, init() needs to be called before sending notifications
 */
export default class OneSignalProvider extends ProviderHandler {
  private _oneSignal: Client

  async init() {
    const ProviderModel = Container.get('providerModel') as Models.ProviderModel
    const item = await ProviderModel.findOne({
      name: 'onesignal',
    })
    if (!item) throw new Error('OneSignal provider record not found')

    const { APP_ID, API_KEY } = item.provider_configuration
    const client = new Client(APP_ID, API_KEY)
    this._oneSignal = client
  }

  async sendNotification(notification: INotification): Promise<boolean> {
    if (!this._oneSignal)
      throw new Error(
        'Class not initialized, init() needs to be called before sending notifications',
      )

    const receivers = (notification.targets as ITarget[])?.map(
      target => target.id_on_provider,
    )

    try {
      await this._oneSignal.createNotification({
        headings: { en: notification.title },
        subtitle: { en: notification.title },
        contents: { en: notification.body },
        include_player_ids: receivers,
      })
      return true
    } catch (error) {
      this.logger?.error(error?.body)
      throw error
    }
  }
}
