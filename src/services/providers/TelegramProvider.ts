import { INotification } from '../../interfaces/INotification'
import ProviderHandler from './ProviderHandler'
import axios from 'axios'
import Container from 'typedi'

/**
 * Class to send Telegram notifications, init() needs to be called before sending notifications
 */
export default class TelegramProvider extends ProviderHandler {
  private _url: string

  async init() {
    const ProviderModel = Container.get('providerModel') as Models.ProviderModel
    const item = await ProviderModel.findOne({
      name: 'telegram',
    })

    if (!item) throw new Error('Telegram provider record not found')

    const { URL } = item.provider_configuration
    this._url = URL
  }

  async sendNotification(notification: INotification): Promise<boolean> {
    if (!this._url)
      throw new Error(
        'Class not initialized, init() needs to be called before sending notifications',
      )
    try {
      await axios.post(this._url, {
        message: notification.body,
      })
      return true
    } catch (error) {
      this.logger?.error(error?.response?.body?.errors)
      throw error
    }
  }
}
