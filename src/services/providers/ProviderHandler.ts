import { Inject } from 'typedi'
import { Logger } from 'winston'
import { INotification } from '../../interfaces/INotification'

export default abstract class {
  constructor(@Inject('logger') protected logger?: Logger) {}

  abstract sendNotification(notification: INotification): Promise<boolean>
}
