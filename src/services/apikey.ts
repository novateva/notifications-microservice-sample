import { IApiKeyInputDTO } from '../interfaces/IApiKey'
import { Inject, Service } from 'typedi'
import mongoose from 'mongoose'
import CryptoJs from 'crypto-js'
import config from '../config'

@Service()
export default class ApiKeyService {
  constructor(@Inject('apiKeyModel') private apiKeyModel: Models.ApiKeyModel) {}

  async create(apiKey: IApiKeyInputDTO) {
    const _id = mongoose.Types.ObjectId()
    const hash = CryptoJs.AES.encrypt(
      _id.toHexString(),
      config.appSecret,
    ).toString()

    const apiKeyRecord = await this.apiKeyModel.create({
      ...apiKey,
      _id,
      hash,
    })
    return apiKeyRecord
  }

  async delete(id: string) {
    const apiKeyRecord = await this.apiKeyModel.findByIdAndDelete(id)
    return apiKeyRecord
  }

  async index() {
    const records = await this.apiKeyModel.find({})
    return records
  }

  async show(id: string) {
    const apiKeyRecord = await this.apiKeyModel.findById(id)
    return apiKeyRecord
  }
}
