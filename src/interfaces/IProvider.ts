export interface IProvider {
  _id: string
  name: string
  executer: string
  provider_configuration: any
}

export interface IProviderInputDTO {
  name: string
  executer: string
  provider_configuration: any
  target?: string
  notification?: string
}
