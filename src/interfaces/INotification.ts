import { IApiKey } from './IApiKey'
import { IProvider } from './IProvider'
import { ITarget } from './ITarget'

export interface INotification {
  _id: string
  _notification_id: string
  title: string
  type: string
  status: string
  body_requested: string
  error_logs: string
  body: string
  template_id: string
  data: any
  internal_user_id: number
  executer: string
  targets?: string[] | ITarget[]
  providers?: string[] | IProvider[]
}

export interface INotificationInputDTO {
  _notification_id: string
  title: string
  type: string
  status: string
  body_requested: string
  error_logs: string
  body: string
  template_id: string
  data: any
  internal_user_id: number
  executer: string
  apikey?: string | IApiKey
  targets?: string[] | ITarget[]
  providers?: string[] | IProvider[]
}
