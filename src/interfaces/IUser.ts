export interface IUser {
  _id: string
  username: string
  email: string
  password: string
  status: string
  salt: string
}

export interface IUserInputDTO {
  username: string
  email: string
  password: string
}
