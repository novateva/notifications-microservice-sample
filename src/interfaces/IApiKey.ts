import { INotification } from './INotification'
import { IUser } from './IUser'

export interface IApiKey {
  _id: string
  hash: string
  expiration_date: Date
  status: string
  user: string | IUser
  notifications?: string[] | INotification[]
}

export interface IApiKeyInputDTO {
  user: string | IUser
  notifications?: string[] | INotification[]
}
