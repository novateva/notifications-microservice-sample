export interface ITarget {
  _id: string
  name: string
  id_on_provider: string
}
