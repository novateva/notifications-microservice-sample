/*!

=========================================================
* Paper Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.Solidusotc.com/product/paper-dashboard-react
* Copyright 2021 Microservice Notification (https://www.Solidusotc.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/main/LICENSE.md)

* Coded by Microservice Notification

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Login from 'views/Login.js'
import { ApiKeys, AddApiKeys } from 'views/ApiKeys'
import { Users, AddUsers } from 'views/Users'
import { Providers } from 'views/Providers'
import { AddProviders } from 'views/Providers'
import { Notifications } from 'views/Notifications'
import { AddNotifications } from 'views/Notifications'

var routes = [
  {
    path: '/users',
    name: 'Users',
    icon: 'nc-icon nc-single-02',
    component: Users,
    layout: '/admin',
    show_dashboard: true,
  },
  {
    path: '/add-user',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddUsers,
    layout: '/admin',
  },
  {
    path: '/edit-user',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddUsers,
    layout: '/admin',
  },
  {
    path: '/api-keys',
    name: 'Api keys',
    icon: 'nc-icon nc-key-25',
    component: ApiKeys,
    layout: '/admin',
    show_dashboard: true,
  },
  {
    path: '/add-api-key',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddApiKeys,
    layout: '/admin',
  },
  {
    path: '/edit-api-key',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddApiKeys,
    layout: '/admin',
  },
  {
    path: '/providers',
    name: 'Providers',
    icon: 'nc-icon nc-globe',
    component: Providers,
    layout: '/admin',
    show_dashboard: true,
  },
  {
    path: '/add-provider',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddProviders,
    layout: '/admin',
  },
  {
    path: '/edit-provider',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddProviders,
    layout: '/admin',
  },
  {
    path: '/notifications',
    name: 'Notifications',
    icon: 'nc-icon nc-bulb-63',
    component: Notifications,
    layout: '/admin',
    show_dashboard: true,
  },
  {
    path: '/add-notification',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddNotifications,
    layout: '/admin',
  },
  {
    path: '/edit-notification',
    name: '',
    icon: 'nc-icon nc-caps-small',
    component: AddNotifications,
    layout: '/admin',
  },
  {
    path: '/login',
    name: 'Login',
    icon: 'nc-icon nc-caps-small',
    component: Login,
    layout: '/auth',
  },
]
export default routes
