/*!

=========================================================
* Paper Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.Solidusotc.com/product/paper-dashboard-react
* Copyright 2021 Microservice Notification (https://www.Solidusotc.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/main/LICENSE.md)

* Coded by Microservice Notification

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from 'react'
import { Container, Row } from 'reactstrap'
// used for making the prop types of this component
import PropTypes from 'prop-types'

function Footer(props) {
  return (
    <footer className={'footer' + (props.default ? ' footer-default' : '')}>
      <Container fluid={props.fluid ? true : false}>
        <Row>
          <nav className="footer-nav">
            <ul>
              <li>
                <a href="https://www.Solidusotc.com" target="_blank">
                  Microservice Notification
                </a>
              </li>
              <li>
                <a href="https://blog.Solidusotc.com" target="_blank">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://www.Solidusotc.com/license" target="_blank">
                  Licenses
                </a>
              </li>
            </ul>
          </nav>
          <div className="credits ml-auto">
            <div className="copyright">
              &copy; {1900 + new Date().getYear()}, made with{' '}
              <i className="fa fa-heart heart" /> by Microservice Notification
            </div>
          </div>
        </Row>
      </Container>
    </footer>
  )
}

Footer.propTypes = {
  default: PropTypes.bool,
  fluid: PropTypes.bool,
}

export default Footer
