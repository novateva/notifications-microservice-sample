import React from 'react'
import { Link } from 'react-router-dom'
import 'font-awesome/css/font-awesome.min.css'
import backgroundImg from '../../assets/img/fabio-mangione.488bc249.jpg'

const Authentication = ({ children }) => {
  return (
    <div className="wrapper wrapper-full-page">
      <div className="full-page section-image">
        <div className="login-page">
          <div className="container">
            <div className="row">
              <div className="ml-auto mr-auto col-md-6 col-lg-4">
                {children}
              </div>
            </div>
          </div>
          <div
            className="full-page-background"
            style={{ backgroundImage: `url(${backgroundImg})` }}
          ></div>
        </div>
        <footer className="footer">
          <div className="container-fluid">
            <div className="row">
              <nav className="footer-nav">
                <ul>
                  <li>
                    <Link to="https://www.Solidusotc.com">
                      Microservice Notification
                    </Link>
                  </li>
                  <li>
                    <Link to="https://blog.Solidusotc.com">Blog</Link>
                  </li>
                  <li>
                    <Link
                      to="https://www.Solidusotc.com/license"
                      target="_blank"
                    >
                      Licenses
                    </Link>
                  </li>
                </ul>
              </nav>
              <div className="credits ml-auto">
                <span className="copyright">
                  © 2021, made with <i className="fa fa-heart heart"></i> by
                  Microservice Notification
                </span>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
  )
}

export default Authentication
