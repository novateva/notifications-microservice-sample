import React from "react"
import { useHistory } from "react-router-dom"
import { Error } from "../Modals"

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

const Tables = ({ title, children, section, showError, setShowError, errorMessage }) => {
  const history = useHistory()

  const goTo = (action) => {
    history.push(`${action}-${section}`)
  }

  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">{title}</CardTitle>
              </CardHeader>
              <CardBody>
                {showError && (
                  <Error
                    message={errorMessage}
                    onClose={() => setShowError(false)}
                  />
                )}
                <Table responsive>
                  {children}
                </Table>
              </CardBody>
            </Card>
            <button className="btn btn-info" onClick={() => goTo("add")}>Agregar</button>
          </Col>
        </Row>
      </div>
    </>
  )
}

export default Tables