import React, { useState } from 'react'
import useDeepCompareEffect from 'use-deep-compare-effect'
import { Row, Col, Input, Button } from 'reactstrap'

const KeyValue = ({ items, setItems }) => {
  const [renderItems, setRenderItems] = useState([])
  const [key, setKey] = useState('')
  const [value, setValue] = useState('')

  useDeepCompareEffect(() => {
    setRenderItems(Object.entries(items))
  }, [items])

  const saveItem = () => {
    if (key) setItems({
      ...items,
      [key]: value,
    })
  }

  const removeItem = key => {
    const used = Object.keys(items).filter(prop => prop !== key)
    let newObj = {}
    used.forEach(item => newObj[item] = items[item])
    setItems(newObj)
  }

  return (
    <>
      {renderItems.map(([itemKey, itemValue], i) => (
        <Row noGutters key={i}>
          <Col md="4" className="pr-3">
            <Input value={itemKey} readOnly />
          </Col>
          <Col md="7">
            <Input value={itemValue} readOnly />
          </Col>
          <Col md="1">
            <Button
              className="form-control m-0"
              onClick={() => removeItem(itemKey)}
            >
              <i className="nc-icon nc-simple-remove" />
            </Button>
          </Col>
        </Row>
      ))}
      <Row noGutters>
        <Col md="4" className="pr-3">
          <Input placeholder="Key" value={key} type="text" onChange={e => setKey(e.target.value)} />
        </Col>
        <Col md="7">
          <Input placeholder="Value" value={value} type="text" onChange={e => setValue(e.target.value)} />
        </Col>
        <Col md="1">
          <Button className="form-control m-0" onClick={saveItem}>
            <i className="nc-icon nc-simple-add" />
          </Button>
        </Col>
      </Row>
    </>
  )
}

export default KeyValue
