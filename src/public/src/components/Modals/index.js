import React from "react"
import SweetAlert from "react-bootstrap-sweetalert"

export const Success = ({ title, message, onConfirm, onCancel }) => (
  <SweetAlert success title={title} onConfirm={onConfirm} onCancel={onCancel}>
    {message}
  </SweetAlert>
)

export const Error = ({message, onClose}) => (
  <div className="alert alert-danger alert-dismissible show" role="alert">
    <button type="button" className="close" aria-label="Close" onClick={onClose}>
      <span aria-hidden="true">×</span>
    </button>
    <span>
      <b>Error - </b>{message}
    </span>
  </div>
)

export const Danger = ({message, confirmText, title, deleteFile, onCancel}) => (
  <SweetAlert
    danger
    showCancel
    confirmBtnText={confirmText}
    confirmBtnBsStyle="danger"
    title={title}
    onConfirm={deleteFile}
    onCancel={onCancel}
    focusCancelBtn
  >
    {message}
  </SweetAlert>
)