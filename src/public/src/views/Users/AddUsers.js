import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import Forms from '../../components/Forms/Forms'
import { Success, Error } from '../../components/Modals'
import axiosInstance from 'utils/axios'

// reactstrap components
import { Button, FormGroup, Form, Input, Row, Col } from 'reactstrap'
import { setToken } from 'utils/axios'

export const AddUsers = ({ location }) => {
  const isEdit = location.pathname.includes('edit')
  const editUser = location?.state?.user ?? {}
  const editUsername = editUser?.username ?? ''
  const [username, setUsername] = useState(editUsername)
  const [email, setEmail] = useState(editUser?.email ?? '')
  const [password, setPassword] = useState('')
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const history = useHistory()

  const saveUser = async () => {
    try {
      const data = isEdit
        ? {
            id: editUser._id,
            ...(username ? { username } : {}),
            ...(email ? { email } : {}),
            ...(password ? { password } : {}),
          }
        : {
            username,
            email,
            password,
          }
      const action = isEdit ? axiosInstance.put : axiosInstance.post
      const resp = await action(`users`, data)
      resp.statusText === 'OK' && setShowSuccess(true)
    } catch (err) {
      console.log(err)
      setShowError(true)
    }
  }

  useEffect(() => {
    const userData = JSON.parse(localStorage.getItem('SOLIDUS_FILE_USER'))
    if (userData?.token) setToken(userData.token)
  }, [])
  return (
    <Forms title="Add User">
      {showError && (
        <Error
          message={`Something went wrong ${
            isEdit ? 'updating' : 'saving'
          } the user`}
          onClose={() => setShowError(false)}
        />
      )}
      <Form>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Username</label>
              <Input
                value={username}
                placeholder="Username"
                type="text"
                onChange={e => setUsername(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Email</label>
              <Input
                value={email}
                placeholder="Email"
                type="email"
                onChange={e => setEmail(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Password</label>
              <Input
                placeholder="Password"
                autoComplete="off"
                type="password"
                className="form-control"
                onChange={e => setPassword(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <div className="update ml-auto mr-auto">
            <Button
              disabled={
                isEdit
                  ? !username && !email && !password
                  : !username || !email || !password
              }
              className="btn-round"
              color="primary"
              onClick={saveUser}
            >
              Save user
            </Button>
          </div>
        </Row>
      </Form>
      {showSuccess && (
        <Success
          title={isEdit ? 'Updated!' : 'Saved'}
          message={`User ${isEdit ? 'edited' : 'saved'} successfuly!`}
          onConfirm={() => history.push('/admin/users')}
        />
      )}
    </Forms>
  )
}
