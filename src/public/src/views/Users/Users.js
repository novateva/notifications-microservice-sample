import React, { useEffect, useState } from "react"
import Tables from "../../components/Tables/Tables"
import { useHistory } from "react-router-dom"
import { Success, Danger } from "../../components/Modals"
import { setToken } from "utils/axios"
import axiosInstance from "utils/axios"

export const Users = () => {
  const [users, setUsers] = useState([])
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)
  const [user2Delete, setUser2Delete] = useState({})
  const history = useHistory()

  useEffect(() => {
    if (!showSuccess) {
      const userData = JSON.parse(localStorage.getItem("SOLIDUS_FILE_USER"))
      if (userData?.token)
        getUsers(userData.token)
        setToken(userData.token)
    }
  }, [showSuccess])

  const getUsers = async () => {
    const resp = await axiosInstance.get(`users`)
    setUsers(resp?.data ?? [])
  }

  const editUser = (user) => {
    history.push("/admin/edit-user", { user })
  }

  const deleteUser = async () => {
    try {
      const resp = await axiosInstance.delete(`users`, {
        data: {
          id: user2Delete
        }
      })
      resp.status === 200 && setShowSuccess(true)
    } catch (err) {
      setShowError(true)
      setShowDeleteConfirm(false)
    }
  }

  return (
    <Tables
      title="Users"
      section="user"
      showError={showError}
      setShowError={setShowError}
      errorMessage="Something went wrong deleting the user"
    >
      <thead className="text-primary">
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th className="text-right">Actions</th>
        </tr>
      </thead>
      <tbody>
      {users.map((user, i) =>
        <tr key={i}>
          <td>{user.username}</td>
          <td>{user.email}</td>
          <td className="text-right">
            <button type="button" id="tooltip159182735" className="btn-icon btn-right btn btn-success btn-sm" onClick={() => {editUser(user)}}>
              <i className="fa fa-edit"></i>
            </button>
            <button type="button" id="tooltip808966390" className="btn-icon btn-right btn btn-danger btn-sm" onClick={() => {
              setUser2Delete(user._id)
              setShowDeleteConfirm(true)
            }}>
              <i className="fa fa-times"></i>
            </button>
          </td>
        </tr>
      )}
      </tbody>
      {showDeleteConfirm && (
        <Danger
          message="You will not be able to recover this user!"
          confirmText="Yes, delete it!"
          title="Are you sure?"
          deleteFile={deleteUser}
          onCancel={() => setShowDeleteConfirm(false)}
        />
      )}
      {showSuccess && (
        <Success
          title="Deleted"
          message={`User deleted successfuly!`}
          onConfirm={() => {
            setShowError(false)
            setShowSuccess(false)
            setShowDeleteConfirm(false)
          }}
        />
      )}
    </Tables>
  )
}
