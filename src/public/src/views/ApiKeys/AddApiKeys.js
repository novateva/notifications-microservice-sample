import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import Forms from '../../components/Forms/Forms'
import { Success, Error } from '../../components/Modals'
import axiosInstance from 'utils/axios'

// reactstrap components
import { Button, Form, Row, Col, FormGroup, Input } from 'reactstrap'
import { setToken } from 'utils/axios'

export const AddApiKeys = () => {
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const [users, setUsers] = useState([])
  const [user, setUser] = useState('')
  const history = useHistory()

  const saveApiKey = async () => {
    try {
      const data = { user }
      const action = axiosInstance.post
      const resp = await action(`api_key`, data)
      resp.statusText === 'OK' && setShowSuccess(true)
    } catch (err) {
      console.log(err?.response)
      setShowError(true)
    }
  }

  useEffect(() => {
    const userData = JSON.parse(localStorage.getItem('SOLIDUS_FILE_USER'))
    if (userData?.token) setToken(userData.token)

    const getUsers = async () => {
      const resp = await axiosInstance.get(`users`)
      setUsers(resp?.data ?? [])
    }

    getUsers()
  }, [])

  return (
    <Forms title="Add Api Key">
      {showError && (
        <Error
          message={`Something went wrong saving the api key`}
          onClose={() => setShowError(false)}
        />
      )}
      <Form>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>User</label>
              <Input
                value={user}
                placeholder="User"
                type="select"
                onChange={e => setUser(e.target.value)}
              >
                <option value="">Pick one</option>
                {users.map((user, i) => (
                  <option key={i} value={user._id}>
                    {user.username}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <div className="update ml-auto mr-auto">
            <Button className="btn-round" color="primary" onClick={saveApiKey} disabled={!user}>
              Save api key
            </Button>
          </div>
        </Row>
      </Form>
      {showSuccess && (
        <Success
          title={'Saved'}
          message={`Api key saved successfuly!`}
          onConfirm={() => history.push('/admin/api-keys')}
        />
      )}
    </Forms>
  )
}
