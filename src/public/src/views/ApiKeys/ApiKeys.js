import React, { useEffect, useState } from "react"
import Tables from "../../components/Tables/Tables"
import { Success, Danger } from "../../components/Modals"
import { setToken } from "utils/axios"
import axiosInstance from "utils/axios"

export const ApiKeys = () => {
  const [apiKeys, setApiKeys] = useState([])
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)
  const [apiKey2Delete, setApiKey2Delete] = useState({})

  useEffect(() => {
    if (!showSuccess) {
      const userData = JSON.parse(localStorage.getItem("SOLIDUS_FILE_USER"))
      if (userData?.token)
        getApiKeys(userData.token)
        setToken(userData.token)
    }
  }, [showSuccess])

  const getApiKeys = async () => {
    const resp = await axiosInstance.get(`api_key`)
    setApiKeys(resp?.data ?? [])
  }

  const deleteApiKey = async () => {
    try {
      const resp = await axiosInstance.delete(`api_key`, {
        data: {
          id: apiKey2Delete
        }
      })
      resp.status === 200 && setShowSuccess(true)
    } catch (err) {
      setShowError(true)
      setShowDeleteConfirm(false)
    }
  }

  return (
    <Tables
      title="User Api Keys"
      section="api-key"
      showError={showError}
      setShowError={setShowError}
      errorMessage="Something went wrong deleting the api key"
    >
      <thead className="text-primary">
        <tr>
          <th>Hash</th>
          <th>Status</th>
          <th className="text-right">Actions</th>
        </tr>
      </thead>
      <tbody>
      {apiKeys.map((apiKey, i) =>
        <tr key={i}>
          <td>{apiKey.hash}</td>
          <td>{apiKey.status}</td>
          <td className="text-right">
            <button type="button" id="tooltip808966390" className="btn-icon btn-right btn btn-danger btn-sm" onClick={() => {
              setApiKey2Delete(apiKey._id)
              setShowDeleteConfirm(true)
            }}>
              <i className="fa fa-times"></i>
            </button>
          </td>
        </tr>
      )}
      </tbody>
      {showDeleteConfirm && (
        <Danger
          message="You will not be able to recover this api key!"
          confirmText="Yes, delete it!"
          title="Are you sure?"
          deleteFile={deleteApiKey}
          onCancel={() => setShowDeleteConfirm(false)}
        />
      )}
      {showSuccess && (
        <Success
          title="Deleted"
          message={`Api key deleted successfuly!`}
          onConfirm={() => {
            setShowError(false)
            setShowSuccess(false)
            setShowDeleteConfirm(false)
          }}
        />
      )}
    </Tables>
  )
}
