import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import Forms from '../../components/Forms/Forms'
import { Success, Error } from '../../components/Modals'
import axiosInstance from 'utils/axios'

// reactstrap components
import { Button, FormGroup, Form, Input, Row, Col } from 'reactstrap'
import { setToken } from 'utils/axios'
import KeyValue from 'components/Forms/KeyValue'

export const AddProviders = ({ location }) => {
  const isEdit = location.pathname.includes('edit')
  const editProvider = location?.state?.provider ?? {}
  const [formData, setFormData] = useState({
    name: editProvider?.name ?? '',
    executer: editProvider?.executer ?? '',
    provider_configuration: editProvider?.provider_configuration ?? {},
  })
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const history = useHistory()

  const formOnChange = e => {
    setFormData(prev => ({
      ...prev,
      [e.target.name]: e.target.value,
    }))
  }

  const setConfig = config => {
    setFormData(prev => ({
      ...prev,
      provider_configuration: config,
    }))
  }

  const isFilled = () => {
    const { name, executer } = formData
    return isEdit ? !name && !executer : !name || !executer
  }

  const saveProvider = async () => {
    try {
      const data = isEdit
        ? {
            id: editProvider._id,
            ...formData,
          }
        : formData
      const action = isEdit ? axiosInstance.put : axiosInstance.post
      const resp = await action(`providers`, data)
      resp.statusText === 'OK' && setShowSuccess(true)
    } catch (err) {
      console.log(err)
      setShowError(true)
    }
  }

  useEffect(() => {
    const providerData = JSON.parse(localStorage.getItem('SOLIDUS_FILE_USER'))
    if (providerData?.token) setToken(providerData.token)
  }, [])
  return (
    <Forms title="Add Provider">
      {showError && (
        <Error
          message={`Something went wrong ${
            isEdit ? 'updating' : 'saving'
          } the provider`}
          onClose={() => setShowError(false)}
        />
      )}
      <Form>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Name</label>
              <Input
                value={formData.name}
                name="name"
                placeholder="Name"
                type="text"
                onChange={formOnChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Executer</label>
              <Input
                value={formData.executer}
                name="executer"
                placeholder="Executer"
                type="text"
                onChange={formOnChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="12">
            <FormGroup>
              <label>Provider configuration</label>
              <KeyValue
                items={formData.provider_configuration}
                setItems={setConfig}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <div className="update ml-auto mr-auto">
            <Button
              disabled={isFilled()}
              className="btn-round"
              color="primary"
              onClick={saveProvider}
            >
              Save provider
            </Button>
          </div>
        </Row>
      </Form>
      {showSuccess && (
        <Success
          title={isEdit ? 'Updated!' : 'Saved'}
          message={`Provider ${isEdit ? 'edited' : 'saved'} successfuly!`}
          onConfirm={() => history.push('/admin/providers')}
        />
      )}
    </Forms>
  )
}
