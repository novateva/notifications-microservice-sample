import React, { useEffect, useState } from "react"
import Tables from "../../components/Tables/Tables"
import { useHistory } from "react-router-dom"
import { Success, Danger } from "../../components/Modals"
import { setToken } from "utils/axios"
import axiosInstance from "utils/axios"

export const Providers = () => {
  const [providers, setProviders] = useState([])
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)
  const [provider2Delete, setProvider2Delete] = useState({})
  const history = useHistory()

  useEffect(() => {
    if (!showSuccess) {
      const providerData = JSON.parse(localStorage.getItem("SOLIDUS_FILE_USER"))
      if (providerData?.token)
        getProviders(providerData.token)
        setToken(providerData.token)
    }
  }, [showSuccess])

  const getProviders = async () => {
    const resp = await axiosInstance.get(`providers`)
    setProviders(resp?.data ?? [])
  }

  const editProvider = (provider) => {
    history.push("/admin/edit-provider", { provider })
  }

  const deleteProvider = async () => {
    try {
      const resp = await axiosInstance.delete(`providers`, {
        data: {
          id: provider2Delete
        }
      })
      resp.status === 200 && setShowSuccess(true)
    } catch (err) {
      setShowError(true)
      setShowDeleteConfirm(false)
    }
  }

  return (
    <Tables
      title="Providers"
      section="provider"
      showError={showError}
      setShowError={setShowError}
      errorMessage="Something went wrong deleting the provider"
    >
      <thead className="text-primary">
        <tr>
          <th>Name</th>
          <th>Executer</th>
          <th className="text-right">Actions</th>
        </tr>
      </thead>
      <tbody>
      {providers.map((provider, i) =>
        <tr key={i}>
          <td>{provider.name}</td>
          <td>{provider.executer}</td>
          <td className="text-right">
            <button type="button" id="tooltip159182735" className="btn-icon btn-right btn btn-success btn-sm" onClick={() => {editProvider(provider)}}>
              <i className="fa fa-edit"></i>
            </button>
            <button type="button" id="tooltip808966390" className="btn-icon btn-right btn btn-danger btn-sm" onClick={() => {
              setProvider2Delete(provider._id)
              setShowDeleteConfirm(true)
            }}>
              <i className="fa fa-times"></i>
            </button>
          </td>
        </tr>
      )}
      </tbody>
      {showDeleteConfirm && (
        <Danger
          message="You will not be able to recover this provider!"
          confirmText="Yes, delete it!"
          title="Are you sure?"
          deleteFile={deleteProvider}
          onCancel={() => setShowDeleteConfirm(false)}
        />
      )}
      {showSuccess && (
        <Success
          title="Deleted"
          message={`Provider deleted successfuly!`}
          onConfirm={() => {
            setShowError(false)
            setShowSuccess(false)
            setShowDeleteConfirm(false)
          }}
        />
      )}
    </Tables>
  )
}
