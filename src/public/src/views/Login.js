import React, { useState } from "react";
import Authentication from "../components/Authentication/Authentication"
import { useHistory } from "react-router-dom"
import { Error } from "../components/Modals"
import axiosInstance from "utils/axios";

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [showError, setShowError] = useState(false)
  const history = useHistory()

  const login = async () => {
    try {
      const resp = await axiosInstance.post(`auth/signin`, {
        email, password
      })
      localStorage.setItem("SOLIDUS_FILE_USER", JSON.stringify(resp.data))
      history.push('/')
    } catch(err) {
      console.log("err", err);
      setShowError(true);
    }
  }

  return (
    <Authentication>
      <form action="" method="" className="form">
        <div className="card-login card">
          <div className="card-header">
            <div className="card-header">
              <h3 className="header text-center">Login</h3>
            </div>
          </div>
          <div className="card-body">
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="nc-icon nc-single-02"></i>
                </span>
              </div>
              <input
                placeholder="Email..."
                type="text"
                className="form-control"
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="nc-icon nc-key-25"></i>
                </span>
              </div>
              <input
                placeholder="Password"
                autoComplete="off"
                type="password"
                className="form-control"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
          </div>
          <div className="card-footer">
            <div onClick={login} className="btn-round mb-3 btn btn-warning btn-block">Get Started</div>
          </div>
        </div>
      </form>
      {showError && <Error message="Couldn't login" onClose={() => setShowError(false)} />}
    </Authentication>
  )
}

export default Login
