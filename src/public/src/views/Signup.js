import React, { useState } from "react";
import Authentication from "../components/Authentication/Authentication"
import 'font-awesome/css/font-awesome.min.css';
import { useHistory } from "react-router-dom"
import { Error } from "../components/Modals"
import axiosInstance from "utils/axios";

const Signup = () => {
  const [checked, setChecked] = useState(false);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showError, setShowError] = useState(false)
  const history = useHistory()

  const signup = async () => {
    try{
      const resp = await axiosInstance.post(`auth/signup`, {
        username, email, password
      })
      localStorage.setItem("SOLIDUS_FILE_USER", JSON.stringify(resp.data))
      history.push('/')
    } catch(err) {
      console.log("err: ", err);
      setShowError(true);
    }

  }

  return (
    <Authentication>
      <div className="card-signup text-center card">
        <div className="card-header">
          <h4 className="card-title">Register</h4>
          <div className="social">
            <button className="btn-icon btn-round btn btn-twitter">
              <i className="fa fa-twitter"></i>
            </button>
            <button className="btn-icon btn-round btn btn-google">
              <i className="fa fa-google"></i>
            </button>
            <button className="btn-icon btn-round btn btn-facebook">
              <i className="fa fa-facebook-f"></i>
            </button>
            <p className="card-description">or be classical</p>
          </div>
        </div>
        <div className="card-body">
          <form action="" method="" className="form">
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="nc-icon nc-single-02"></i>
                </span>
              </div>
              <input
                placeholder="Name..."
                type="text"
                className="form-control"
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="nc-icon nc-email-85"></i>
                </span>
              </div>
              <input
                placeholder="Email..."
                type="email"
                className="form-control"
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="nc-icon nc-key-25"></i>
                </span>
              </div>
              <input
                placeholder="Password"
                autoComplete="off"
                type="password"
                className="form-control"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div className="text-left form-check">
              <label className="form-check-label">
                <input
                  type="checkbox"
                  className="form-check-input"
                  checked={checked}
                  onChange={() => setChecked(!checked)}
                />
                <span className="form-check-sign"></span>I agree to the <a href="#pablo">terms and conditions</a>.
              </label>
            </div>
          </form>
        </div>
        <div className="card-footer">
          <div onClick={signup} className="btn-round btn btn-info">Get Started</div>
        </div>
      </div>
      {showError && <Error message="Couldn't register user" onClose={() => setShowError(false)} />}
    </Authentication>

  )
}

export default Signup
