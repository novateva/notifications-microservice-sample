import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import Forms from '../../components/Forms/Forms'
import { Success, Error } from '../../components/Modals'
import axiosInstance from 'utils/axios'

// reactstrap components
import { Button, FormGroup, Form, Input, Row, Col } from 'reactstrap'
import { setToken } from 'utils/axios'
import KeyValue from 'components/Forms/KeyValue'

export const AddNotifications = ({ location }) => {
  const isEdit = location.pathname.includes('edit')
  const editNotification = location?.state?.notification ?? {}
  const [formData, setFormData] = useState({
    title: editNotification?.title ?? '',
    type: editNotification?.type ?? 'email',
    status: editNotification?.status ?? 'going',
    body: editNotification?.body ?? '',
    template_id: editNotification?.template_id ?? '',
    data: editNotification?.data ?? {},
    providers: editNotification?.providers ?? [],
  })
  const [providers, setProviders] = useState([])
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const history = useHistory()

  const formOnChange = e => {
    setFormData(prev => ({
      ...prev,
      [e.target.name]: e.target.value,
    }))
  }

  const providersOnChange = e => {
    if (formData.providers.includes(e.target.value))
      setFormData(prev => ({
        ...prev,
        providers: prev.providers.filter(item => item !== e.target.value),
      }))
    else
      setFormData(prev => ({
        ...prev,
        providers: [...prev.providers, e.target.value],
      }))
  }

  const setData = config => {
    setFormData(prev => ({
      ...prev,
      data: config,
    }))
  }

  useEffect(() => {
    console.log(formData.providers)
  }, [formData.providers])

  const isFilled = () => {
    const { title, body, template_id } = formData
    return isEdit
      ? !title && !body && !template_id
      : !title || !body || !template_id
  }

  const saveNotification = async () => {
    try {
      const data = isEdit
        ? {
            id: editNotification._id,
            ...formData,
          }
        : formData
      const action = isEdit ? axiosInstance.put : axiosInstance.post
      const resp = await action(`notifications`, data)
      resp.statusText === 'OK' && setShowSuccess(true)
    } catch (err) {
      console.log(err)
      setShowError(true)
    }
  }

  useEffect(() => {
    const userData = JSON.parse(localStorage.getItem('SOLIDUS_FILE_USER'))
    if (userData?.token) setToken(userData.token)

    const getProviders = async () => {
      const resp = await axiosInstance.get(`providers`)
      setProviders(resp?.data ?? [])
    }

    getProviders()
  }, [])

  return (
    <Forms title="Add Notification">
      {showError && (
        <Error
          message={`Something went wrong ${
            isEdit ? 'updating' : 'saving'
          } the notification`}
          onClose={() => setShowError(false)}
        />
      )}
      <Form>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Title</label>
              <Input
                value={formData.title}
                name="title"
                placeholder="Title"
                type="text"
                onChange={formOnChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Type</label>
              <Input
                value={formData.type}
                name="type"
                placeholder="Type"
                type="select"
                onChange={formOnChange}
              >
                <option value="email">Email</option>
                <option value="push">Push</option>
                <option value="telegram">Telegram</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Status</label>
              <Input
                value={formData.status}
                name="status"
                placeholder="Status"
                type="select"
                onChange={formOnChange}
              >
                <option value="going">Going</option>
                <option value="paused">Paused</option>
                <option value="error">Error</option>
                <option value="deleted">Deleted</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Body</label>
              <Input
                value={formData.body}
                name="body"
                placeholder="Body"
                type="text"
                onChange={formOnChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Template Id</label>
              <Input
                value={formData.template_id}
                name="template_id"
                placeholder="Template Id"
                type="text"
                onChange={formOnChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="12">
            <FormGroup>
              <label>Data</label>
              <KeyValue items={formData.data} setItems={setData} />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col className="pr-1" md="6">
            <FormGroup>
              <label>Providers</label>
              <Input
                value={formData.providers}
                placeholder="Providers"
                type="select"
                onChange={providersOnChange}
                multiple
              >
                {providers.map((provider, i) => (
                  <option
                    key={i}
                    value={provider._id}
                    className={
                      formData.providers.includes(provider._id) ? 'border' : ''
                    }
                  >
                    {provider.name}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <div className="update ml-auto mr-auto">
            <Button
              disabled={isFilled()}
              className="btn-round"
              color="primary"
              onClick={saveNotification}
            >
              Save notification
            </Button>
          </div>
        </Row>
      </Form>
      {showSuccess && (
        <Success
          title={isEdit ? 'Updated!' : 'Saved'}
          message={`Notification ${isEdit ? 'edited' : 'saved'} successfuly!`}
          onConfirm={() => history.push('/admin/notifications')}
        />
      )}
    </Forms>
  )
}
