import React, { useEffect, useState } from "react"
import Tables from "../../components/Tables/Tables"
import { useHistory } from "react-router-dom"
import { Success, Danger } from "../../components/Modals"
import { setToken } from "utils/axios"
import axiosInstance from "utils/axios"

export const Notifications = () => {
  const [notifications, setNotifications] = useState([])
  const [showSuccess, setShowSuccess] = useState(false)
  const [showError, setShowError] = useState(false)
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)
  const [notification2Delete, setNotification2Delete] = useState({})
  const history = useHistory()

  useEffect(() => {
    if (!showSuccess) {
      const notificationData = JSON.parse(localStorage.getItem("SOLIDUS_FILE_USER"))
      if (notificationData?.token)
        getNotifications(notificationData.token)
        setToken(notificationData.token)
    }
  }, [showSuccess])

  const getNotifications = async () => {
    const resp = await axiosInstance.get(`notifications`)
    setNotifications(resp?.data ?? [])
  }

  const editNotification = (notification) => {
    history.push("/admin/edit-notification", { notification })
  }

  const deleteNotification = async () => {
    try {
      const resp = await axiosInstance.delete(`notifications`, {
        data: {
          id: notification2Delete
        }
      })
      resp.status === 200 && setShowSuccess(true)
    } catch (err) {
      setShowError(true)
      setShowDeleteConfirm(false)
    }
  }

  return (
    <Tables
      title="Notifications"
      section="notification"
      showError={showError}
      setShowError={setShowError}
      errorMessage="Something went wrong deleting the notification"
    >
      <thead className="text-primary">
        <tr>
          <th>Id</th>
          <th>Status</th>
          <th>Type</th>
          <th>Error log</th>
          <th>Body requested</th>
          <th className="text-right">Actions</th>
        </tr>
      </thead>
      <tbody>
      {notifications.map((notification, i) =>
        <tr key={i}>
          <td>{notification._notification_id}</td>
          <td>{notification.status}</td>
          <td>{notification.type}</td>
          <td>{notification.error_logs}</td>
          <td>{notification.body_requested}</td>
          <td className="text-right">
            <button type="button" id="tooltip159182735" className="btn-icon btn-right btn btn-success btn-sm" onClick={() => {editNotification(notification)}}>
              <i className="fa fa-edit"></i>
            </button>
            <button type="button" id="tooltip808966390" className="btn-icon btn-right btn btn-danger btn-sm" onClick={() => {
              setNotification2Delete(notification._id)
              setShowDeleteConfirm(true)
            }}>
              <i className="fa fa-times"></i>
            </button>
          </td>
        </tr>
      )}
      </tbody>
      {showDeleteConfirm && (
        <Danger
          message="You will not be able to recover this notification!"
          confirmText="Yes, delete it!"
          title="Are you sure?"
          deleteFile={deleteNotification}
          onCancel={() => setShowDeleteConfirm(false)}
        />
      )}
      {showSuccess && (
        <Success
          title="Deleted"
          message={`Notification deleted successfuly!`}
          onConfirm={() => {
            setShowError(false)
            setShowSuccess(false)
            setShowDeleteConfirm(false)
          }}
        />
      )}
    </Tables>
  )
}
