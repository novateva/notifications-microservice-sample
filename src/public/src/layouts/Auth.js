import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import DemoNavbar from "components/Navbars/DemoNavbar.js";

import routes from "routes.js";

const Login =(props) => {
  if (localStorage.getItem('SOLIDUS_FILE_USER'))
    return <Redirect to="/admin/users" />
  return (
    <>
      <DemoNavbar login {...props} />
        <Switch>
          {routes.map((prop, key) => {
            return (
              <Route
                path={prop.layout + prop.path}
                component={prop.component}
                key={key}
              />
            );
          })}
        </Switch>
    </>
  )
}

export default Login;
