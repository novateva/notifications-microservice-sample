import axios from 'axios'

export let token = ''

export const setToken = _token => {
  token = _token
}

const axiosInstance = axios.create({
  baseURL: `${process.env.REACT_APP_PUBLIC_URL}/api/`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  }
})

axiosInstance.interceptors.request.use(x => {
  x.headers.Authorization = `Bearer ${token}`
  return x
})

export default axiosInstance
