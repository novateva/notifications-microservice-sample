/*!

=========================================================
* Paper Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.Solidusotc.com/product/paper-dashboard-react
* Copyright 2021 Microservice Notification (https://www.Solidusotc.com)

* Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard-react/blob/main/LICENSE.md)

* Coded by Microservice Notification

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.css'
import 'assets/scss/paper-dashboard.scss?v=1.3.0'
import 'assets/demo/demo.css'
import 'perfect-scrollbar/css/perfect-scrollbar.css'

import AdminLayout from 'layouts/Admin.js'
import Auth from 'layouts/Auth.js'
import { setToken } from 'utils/axios'

const userData = JSON.parse(localStorage.getItem('SOLIDUS_FILE_USER'))
if (userData?.token) setToken(userData.token)

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/admin" render={props => <AdminLayout {...props} />} />
      <Route path="/auth" render={props => <Auth {...props} />} />
      <Redirect to="/auth/login" />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root'),
)
