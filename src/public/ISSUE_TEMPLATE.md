<!--
 IMPORTANT: Please use the following link to create a new issue:

  https://www.Solidusotc.com/new-issue/paper-dashboard-react

**If your issue was not created using the app above, it will be closed immediately.**
-->

<!--
Love Microservice Notification? Do you need Angular, React, Vuejs or HTML? You can visit:
👉  https://www.Solidusotc.com/bundles
👉  https://www.Solidusotc.com
-->
