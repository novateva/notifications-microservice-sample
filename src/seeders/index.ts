import dependencyInjectorLoader from '../loaders/dependencyInjector'
import LoggerInstance from '../loaders/logger'
import mongooseLoader from '../loaders/mongoose'
import { apiKeyModel, userModel, providerModel } from '../models'
import mongoose from 'mongoose'
import userData from './user.json'
import providerData from './provider.json'
import AppSeeder from './AppSeeder'
const seed = async () => {
  const mongoConnection = await mongooseLoader()
  await dependencyInjectorLoader({
    mongoConnection: mongoConnection,
    models: {
      apiKeyModel,
      userModel,
      providerModel,
    },
  })

  const appSeeder = new AppSeeder()
  await appSeeder.run([userData, providerData])
  await mongoose.disconnect()
}

seed().then(() => LoggerInstance.info('Seeding complete!'))
