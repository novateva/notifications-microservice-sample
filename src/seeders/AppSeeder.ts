import { Model } from 'mongoose'
import Container from 'typedi'
import LoggerInstance from '../loaders/logger'

type SeederData = { model: string; data: any[] }

class ApplicationSeeder {
  async run(data: SeederData[]) {
    const operations = data.map(async item => {
      LoggerInstance.info(`Seeding ${item.model}...`)
      const usedModel: Model<any, any> = Container.get(item.model)
      return await usedModel.insertMany(item.data)
    })

    await Promise.all(operations)
  }
}

export default ApplicationSeeder
