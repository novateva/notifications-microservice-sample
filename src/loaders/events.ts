//Here we import all events
//import '../subscribers/user'
import { EventDispatcher } from '@foxandfly/ts-event-dispatcher'
import { NotificationSubscriber } from '../subscribers/notification'
import { UserSubscriber } from '../subscribers/user'

const dispatcher = new EventDispatcher()

NotificationSubscriber(dispatcher)
UserSubscriber(dispatcher)

export { dispatcher }
