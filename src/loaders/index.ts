import expressLoader from './express'
import dependencyInjectorLoader from './dependencyInjector'
import mongooseLoader from './mongoose'
import jobsLoader from './jobs'
import Logger from './logger'
import {
  apiKeyModel,
  notificationModel,
  providerModel,
  targetModel,
  userModel,
} from '../models'

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader()
  Logger.info('DB loaded and connected!')

  /**
   * WTF is going on here?
   *
   * We are injecting the mongoose models into the DI container.
   * I know this is controversial but will provide a lot of flexibility at the time
   * of writing unit tests, just go and check how beautiful they are!
   */

  // It returns the agenda instance because it's needed in the subsequent loaders
  const { agenda } = await dependencyInjectorLoader({
    mongoConnection,
    models: {
      apiKeyModel,
      notificationModel,
      providerModel,
      targetModel,
      userModel,
    },
  })
  Logger.info('Dependency Injector loaded')

  await jobsLoader({ agenda })
  Logger.info('Jobs loaded')

  await expressLoader({ app: expressApp })
  Logger.info('Express loaded')
}
