import {
  OneSignalProvider,
  SendgridProvider,
  TelegramProvider,
} from '../services/providers'

export default async () => {
  const onesignal = new OneSignalProvider()
  const sendgrid = new SendgridProvider()
  const telegram = new TelegramProvider()

  await onesignal.init()
  await sendgrid.init()
  await telegram.init()

  return {
    OneSignalProvider: onesignal,
    SendgridProvider: sendgrid,
    TelegramProvider: telegram,
  }
}
