import { Container } from 'typedi'
import LoggerInstance from './logger'
import agendaFactory from './agenda'
import providerLoader from './providers'
import { dispatcher } from './events'

export default async ({
  mongoConnection,
  models,
}: {
  mongoConnection
  models: { [key: string]: any }
}) => {
  try {
    Object.entries(models).forEach(([name, model]) => {
      console.log(name, model)
      Container.set(name, model)
    })

    const { OneSignalProvider, SendgridProvider, TelegramProvider } =
      await providerLoader()

    const agendaInstance = agendaFactory({ mongoConnection })

    Container.set('agendaInstance', agendaInstance)
    Container.set('logger', LoggerInstance)

    LoggerInstance.info('Agenda injected into container')

    Container.set('eventDispatcher', dispatcher)
    Container.set('emailProvider', SendgridProvider)
    Container.set('pushProvider', OneSignalProvider)
    Container.set('telegramProvider', TelegramProvider)

    LoggerInstance.info('Notification providers injected into container')

    return { agenda: agendaInstance }
  } catch (e) {
    LoggerInstance.error('Error on dependency injector loader: %o', e)
    throw e
  }
}
