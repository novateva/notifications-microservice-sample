import { IApiKey } from '../interfaces/IApiKey'
import mongoose, { Schema } from 'mongoose'

const ApiKey = new mongoose.Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },

    notifications: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Notification',
      },
    ],

    hash: String,

    status: {
      type: String,
      enum: ['ACTIVE', 'DISABLED'],
      default: 'ACTIVE',
    },
  },
  { timestamps: true },
)

export default mongoose.model<IApiKey & mongoose.Document>('ApiKey', ApiKey)
