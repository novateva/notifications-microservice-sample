import mongoose, { Schema } from 'mongoose'
import { INotification } from '../interfaces/INotification'

const Notification = new mongoose.Schema(
  {
    _notification_id: {
      type: String,
      required: false,
      index: true,
      default: null,
    },

    providers: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Provider',
      },
    ],

    targets: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Target',
      },
    ],

    title: String,

    type: {
      type: String,
      enum: ['email', 'telegram', 'push'],
      default: 'email',
    },

    status: {
      type: String,
      enum: ['going', 'paused', 'error', 'deleted'],
      default: 'going',
    },

    body_requested: String,

    error_logs: String,

    body: String,

    template_id: String,

    data: {
      type: Schema.Types.Mixed,
    },

    internal_user_id: Number,

    executer: String,
  },
  { timestamps: true },
)

export default mongoose.model<INotification & mongoose.Document>(
  'Notification',
  Notification,
)
