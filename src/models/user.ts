import { IUser } from '../interfaces/IUser'
import mongoose, { Schema } from 'mongoose'

const User = new mongoose.Schema(
  {
    api_keys: [{ type: Schema.Types.ObjectId, ref: 'ApiKey' }],

    username: {
      type: String,
      required: [true, 'Please enter a full name'],
      index: true,
    },

    email: {
      type: String,
      lowercase: true,
      unique: true,
      index: true,
    },

    password: String,

    status: {
      type: String,
      enum: ['A', 'B'],
      default: 'A',
    },

    salt: String,
  },
  { timestamps: true },
)

export default mongoose.model<IUser & mongoose.Document>('User', User)
