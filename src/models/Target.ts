import { ITarget } from '../interfaces/ITarget'
import mongoose, { Schema } from 'mongoose'

const Target = new mongoose.Schema(
  {
    notification: {
      type: Schema.Types.ObjectId,
      ref: 'Notification',
    },

    providers: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Provider',
      },
    ],

    name: String,

    id_on_provider: String,
  },
  { timestamps: true },
)

export default mongoose.model<ITarget & mongoose.Document>('Target', Target)
