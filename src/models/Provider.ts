import { IProvider } from '../interfaces/IProvider'
import mongoose, { Schema } from 'mongoose'

const Provider = new mongoose.Schema(
  {
    target: {
      type: Schema.Types.ObjectId,
      ref: 'Target',
    },

    notification: {
      type: Schema.Types.ObjectId,
      ref: 'Notification',
    },

    name: String,

    executer: String,

    provider_configuration: Schema.Types.Mixed,
  },
  { timestamps: true },
)

export default mongoose.model<IProvider & mongoose.Document>(
  'Provider',
  Provider,
)
