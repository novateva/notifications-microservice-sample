#!/bin/bash
npm run build-front
npm run build
cp -r src/public/build/* build/public/build/
cp -r .elasticbeanstalk build
cp -r .platform build
cp package-deploy.json build/package.json
echo "" > build/.env
echo "unsafe-perm=true" > build/.npmrc
cd build
eb deploy
