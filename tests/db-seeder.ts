import {
  apiKeyModel,
  notificationModel,
  providerModel,
  targetModel,
} from '../src/models'
import { apikeyData, notificationData, providerData, targetData } from './seeds'

export default async () => {
  await providerModel.insertMany(providerData)
  await notificationModel.insertMany(notificationData)
  await apiKeyModel.insertMany(apikeyData)
  await targetModel.insertMany(targetData)
}
