import apikeyData from './apikeys.json'
import notificationData from './notitications.json'
import providerData from './providers.json'
import targetData from './targets.json'

export { apikeyData, notificationData, providerData, targetData }
