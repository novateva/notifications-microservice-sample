import ProviderService from '../../src/services/provider'

describe('Provider service unit tests', () => {
  describe('Create', () => {
    it('Should create provider record', async () => {
      const providerModel = {
        create: provider => {
          return {
            ...provider,
            _id: 'mock-id',
          }
        },
      }

      const userInput = {
        name: 'Unit Test',
        executer: 'Testtest',
        provider_configuration: {},
        target: 'asd-id',
        notification: 'qwer-id',
      }

      const providerService = new ProviderService(providerModel)
      const providerRecord = await providerService.create(userInput)

      expect(providerRecord).toBeDefined()
      expect(providerRecord._id).toBeDefined()
      expect(providerRecord.name).toBeDefined()
    })
  })

  describe('Update', () => {
    it('Should update provider record', async () => {
      const providerModel = {
        findByIdAndUpdate: (_, provider) => {
          return {
            ...provider,
            _id: 'mock-id',
          }
        },
      }

      const userInput = {
        name: 'Unit Test',
        executer: 'Testtest',
        provider_configuration: {},
      }

      const providerService = new ProviderService(providerModel)
      const providerRecord = await providerService.update('mock-id', userInput)

      expect(providerRecord).toBeDefined()
      expect(providerRecord._id).toBeDefined()
      expect(providerRecord.name).toBeDefined()
    })
  })
})
