import Container from 'typedi'
import { INotification } from '../../../src/interfaces/INotification'
import LoggerInstance from '../../../src/loaders/logger'
import { notificationModel, providerModel } from '../../../src/models'
import ProviderHandler from '../../../src/services/providers/ProviderHandler'
import ProvidersLoader from '../../../src/loaders/providers'
import dbHandler from '../../db-handler'
import dbSeeder from '../../db-seeder'

let OneSignalProvider: ProviderHandler

beforeAll(async () => {
  await dbHandler.connect()
  Container.set('logger', LoggerInstance)
  Container.set('notificationModel', notificationModel)
  Container.set('providerModel', providerModel)
  await dbSeeder()
  ;({ OneSignalProvider } = await ProvidersLoader())
})
afterAll(async () => await dbHandler.closeDatabase())

describe('OneSignal Provider unit tests', () => {
  describe('Send Notification', () => {
    it('Should send notification successfully', async () => {
      const notification: INotification = {
        _id: 'mock-id',
        _notification_id: 'mock-id2',
        body: 'test notif',
        body_requested: 'any',
        data: {},
        error_logs: 'any',
        executer: 'exe',
        internal_user_id: 1,
        status: 'going',
        template_id: 'd-07e8e84cd23c4cd68df2f17031281a70',
        title: 'Hey',
        type: 'email',
        targets: [
          {
            _id: 'mock',
            name: 'test',
            provider_configuration: {},
            id_on_provider: 'one-signal-device-id',
          },
        ],
      }

      const res = await OneSignalProvider.sendNotification(notification)

      expect(res).toBeTruthy()
    })
  })
})
