import { INotification } from '../../../src/interfaces/INotification'
import ProvidersLoader from '../../../src/loaders/providers'
import dbHandler from '../../db-handler'
import Container from 'typedi'
import dbSeeder from '../../db-seeder'
import { notificationModel, providerModel } from '../../../src/models'
import LoggerInstance from '../../../src/loaders/logger'

let TelegramProvider

beforeAll(async () => {
  await dbHandler.connect()
  Container.set('logger', LoggerInstance)
  Container.set('notificationModel', notificationModel)
  Container.set('providerModel', providerModel)
  await dbSeeder()
  ;({ TelegramProvider } = await ProvidersLoader())
})
afterAll(async () => await dbHandler.closeDatabase())

describe('Telegram Provider unit tests', () => {
  describe('Send Notification', () => {
    it('Should send notification successfully', async () => {
      const notification: INotification = {
        _id: 'mock-id',
        _notification_id: 'mock-id2',
        body: 'test notif',
        body_requested: 'any',
        data: {},
        error_logs: 'any',
        executer: 'exe',
        internal_user_id: 1,
        status: 'going',
        template_id: 'd-07e8e84cd23c4cd68df2f17031281a70',
        title: 'Hey',
        type: 'email',
        targets: [
          {
            _id: 'mock',
            name: 'test',
            provider_configuration: {},
            id_on_provider: 'one-signal-device-id',
          },
        ],
      }

      const res = await TelegramProvider.sendNotification(notification)

      expect(res).toBeTruthy()
    })
  })
})
