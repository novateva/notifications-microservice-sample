import Container from 'typedi'
import dbHandler from '../../db-handler'
import dbSeeder from '../../db-seeder'
import LoggerInstance from '../../../src/loaders/logger'
import { notificationModel } from '../../../src/models'
import { NotificationService } from '../../../src/services'
import { dispatcher } from '../../../src/loaders/events'

jest.setTimeout(30000)

let notificationServiceInstance: NotificationService

const mock_id = 'aaaaaaaaaaaaaaaaaaaaaaaa'

beforeAll(async () => {
  await dbHandler.connect()
  Container.set('logger', LoggerInstance)
  Container.set('notificationModel', notificationModel)
  notificationServiceInstance = new NotificationService(
    notificationModel,
    dispatcher,
  )
})
beforeEach(async () => await dbSeeder())
afterEach(async () => await dbHandler.clearDatabase())
afterAll(async () => await dbHandler.closeDatabase())

describe('Sendgrid handler', () => {
  it('notification send on create', async () => {
    const called = jest.spyOn(dispatcher, 'dispatch')
    const item = await notificationServiceInstance.create(notificationComplete)
    expect(item).toBeDefined()
    expect(called).toBeCalled()
  })
})

//TODO: Refactor this into .json files
const notificationComplete = {
  _notification_id: 'bbbb',
  title: 'asd',
  type: 'email',
  status: 'going',
  data: {},
  body_requested: 'asd',
  error_logs: 'asd',
  body: 'asd',
  template_id: 'd-07e8e84cd23c4cd68df2f17031281a70',
  internal_user_id: 14,
  executer: 'hehe',
  targets: [mock_id],
}
