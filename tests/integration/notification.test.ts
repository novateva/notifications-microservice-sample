import dbHandler from '../db-handler'
import notificationService from '../../src/services/notification'
import notificationModel from '../../src/models/Notification'
import dbSeeder from '../db-seeder'
import { EventDispatcher } from '@foxandfly/ts-event-dispatcher'

jest.mock('@foxandfly/ts-event-dispatcher')

const dispatcher = new EventDispatcher()
const notificationServiceInstance = new notificationService(notificationModel, dispatcher)

jest.setTimeout(30000)

const mock_id = 'aaaaaaaaaaaaaaaaaaaaaaaa'

beforeAll(async () => await dbHandler.connect())
beforeEach(async () => await dbSeeder())
afterEach(async () => await dbHandler.clearDatabase())
afterAll(async () => await dbHandler.closeDatabase())

describe('notification', () => {
  it('can be created correctly', async () => {
    const item = await notificationServiceInstance.create(notificationComplete)
    expect(item.title).toBe(notificationComplete.title)
    expect(item.executer).toBe(notificationComplete.executer)
    expect(item.data).toBe(notificationComplete.data)
  })

  it('can be update correctly', async () => {
    const item = await notificationServiceInstance.update(mock_id, notificationUpdated)
    expect(item.title).toBe(notificationUpdated.title)
  })

  it('can get all', async () => {
    const items = await notificationServiceInstance.index()
    expect(items.length).toBeGreaterThan(0)
  })

  it('can get one', async () => {
    const item = await notificationServiceInstance.show(mock_id)
    expect(item.title).toBe(notificationComplete.title)
  })

  it('can delete', async () => {
    const item = await notificationServiceInstance.delete(mock_id)
    expect(item).not.toBeNull()
  })
})

//TODO: Refactor this into .json files
const notificationComplete = {
  _notification_id: 'bbbb',
  title: 'asd',
  type: 'email',
  status: 'going',
  data: {},
  body_requested: 'asd',
  error_logs: 'asd',
  body: 'asd',
  template_id: 'asd',
  internal_user_id: 14,
  executer: 'hehe',
}

const notificationUpdated = {
  _notification_id: 'bbbb',
  title: 'qwerty',
  type: 'email',
  status: 'going',
  data: {},
  body_requested: 'asd',
  error_logs: 'asd',
  body: 'asd',
  template_id: 'asd',
  internal_user_id: 14,
  executer: 'hehe',
}
