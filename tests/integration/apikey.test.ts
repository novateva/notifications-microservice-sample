import dbHandler from '../db-handler'
import apiKeyService from '../../src/services/apikey'
import apiKeyModel from '../../src/models/ApiKey'
import dbSeeder from '../db-seeder'

const apikeyServiceInstance = new apiKeyService(apiKeyModel)

jest.setTimeout(30000)

const mock_id = 'aaaaaaaaaaaaaaaaaaaaaaaa'

beforeAll(async () => await dbHandler.connect())
beforeEach(async () => await dbSeeder())
afterEach(async () => await dbHandler.clearDatabase())
afterAll(async () => await dbHandler.closeDatabase())

describe('apikey', () => {
  it('can be created correctly', async () => {
    const item = await apikeyServiceInstance.create(apiKeyComplete)
    expect(item.hash).toBe(apiKeyComplete.hash)
    expect(item.expiration_date).toBe(apiKeyComplete.expiration_date)
    expect(item.status).toBe(apiKeyComplete.status)
  })

  it('can delete', async () => {
    const item = await apikeyServiceInstance.delete(mock_id)
    expect(item).not.toBeNull()
  })
})

//TODO: Refactor this into .json files
const apiKeyComplete = {
  hash: 'test-hash',
  expiration_date: new Date(),
  status: 'ACTIVE',
}
