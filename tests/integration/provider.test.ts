import dbHandler from '../db-handler'
import providerService from '../../src/services/provider'
import providerModel from '../../src/models/Provider'
import dbSeeder from '../db-seeder'

const providerServiceInstance = new providerService(providerModel)

jest.setTimeout(30000)

const mock_id = 'aaaaaaaaaaaaaaaaaaaaaaaa'

beforeAll(async () => await dbHandler.connect())
beforeEach(async () => await dbSeeder())
afterEach(async () => await dbHandler.clearDatabase())
afterAll(async () => await dbHandler.closeDatabase())

describe('provider', () => {
  it('can be created correctly', async () => {
    const item = await providerServiceInstance.create(providerComplete)
    expect(item.name).toBe(providerComplete.name)
    expect(item.executer).toBe(providerComplete.executer)
    expect(item.provider_configuration).toBe(
      providerComplete.provider_configuration,
    )
  })

  it('can be update correctly', async () => {
    const item = await providerServiceInstance.update(mock_id, providerUpdated)
    expect(item.name).toBe(providerUpdated.name)
  })

  it('can get all', async () => {
    const items = await providerServiceInstance.index()
    expect(items.length).toBeGreaterThan(0)
  })

  it('can get one', async () => {
    const item = await providerServiceInstance.show(mock_id)
    expect(item.name).toBe(providerComplete.name)
  })

  it('can delete', async () => {
    const item = await providerServiceInstance.delete(mock_id)
    expect(item).not.toBeNull()
  })
})

//TODO: Refactor this into .json files
const providerComplete = {
  executer: 'hehe',
  name: 'sendgrid',
  provider_configuration: {},
}

const providerUpdated = {
  executer: 'hehe',
  name: 'dummy',
  provider_configuration: {},
}
